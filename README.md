Generates fonts for LittlevGL

Fonts in hex format
* http://pelulamu.net/unscii/
* http://unifoundry.com/unifont/index.html

Converting to LittlevGL font:
~~~sh
ruby hex2lvfont.rb unscii-16.hex -h 16
~~~
